**Detalhes da Automação**

* O plano e os cenários de teste se encontra dentro do arquivo Teste PagBrasil.txt.

* A automação esta na pasta de automação e foi criado em cypress somente a escrita. Por não ter acesso a tela não foi possível fazer a execução no browser. Foi craido dois arquivos, um no qual chama as funconalidades do front e validação com a tela, e o outro arquivo que trata somente da automação de API.

* Cypress do Back End é usado para inserção manual dentro da API.

* Não foi necessário utilizar um token na API por que não foi realizado uma requisição com login e senha.

* O \ é a rota principal da URL básica para acessar a tela do front end. (Detalhe: essa URL foi inventado, de fato não há certeza que existe com esse nome).

* Para acessar a URL da API foi criado uma variável dentro de "env" para organizar o acesso da API. Ao ser chamado essa rota dentro da Api é acescentado o endpoint form(que foi inventando para realizar o teste).

* No arquivo de automação do front foi adicionado uma função com o nome de Cypress.Commands.add no qual vai ser chamado a ação do botão "Submit".

